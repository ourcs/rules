import os
import json


def check_ourcs_formatting(obj):
    
    if not isinstance(obj, dict):
        raise ValueError('Expected dict, found:\n{}'.format(obj))
    
    if not 'title' in obj:
        raise ValueError('Object missing "title" key: {}'.format(obj))
    
    if 'contents' in obj:
        if not isinstance(obj['contents'], list):
            raise ValueError('Expected list, found:\n{}'.format(obj))
        
        for content in obj['contents']:
            check_ourcs_formatting(content)


if __name__ == '__main__':
    json_files = [file for file in os.listdir() if file.split('.')[-1] == 'json']
    json_files.remove('test.json')
    
    for file in json_files:
        print('Checking {} ...'.format(file))
        with open(file, encoding = 'utf-8') as file_obj:
            content = json.load(file_obj)
        
        check_ourcs_formatting(content)
    
    print('All files appear valid')

