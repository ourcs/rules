let refDict = {}

function formatJSON (prefix, data, level = 1) {
    let wrapper = {}
    switch (level) {
        case 1:
            wrapper.start = "<h1>"
            wrapper.end = "</h1>"
            break
        case 2:
            wrapper.start = "<p><b>"
            wrapper.end = "</b></p>"
            break
        default:
            wrapper.start = "<p style='text-indent: -1em; padding-left: "+(2*(level-2)+1)+"em'>"
            wrapper.end = "</p>"
            break
    }

    let prefixShown = prefix
    if (level > 2) {
        let prefixes = prefix.split('.')
        prefixShown = prefixes[prefixes.length-1]
    }

    let output = ""
    output += wrapper.start + prefixShown + ".&nbsp;&nbsp;" + data.title + wrapper.end
    if (data.content) {
        output += "<p>" + data.content + "</p>"
    }
    if (data.ref) {
        refDict[data.ref] = prefix
        output += "<a style='display: block; position: relative; top: -200px; visibility: hidden' name='" + data.ref + "' />"
    }
    if (data.contents) {
        for (let i = 0; i < data.contents.length; i++) {
            output += formatJSON(prefix + "." + getNumbering(i, level), data.contents[i], level + 1)
        }
    }
    if (data.continued) {
        output += "<p style='padding-left: "+(2*(level-2)+1)+"em'>" + data.continued + "</p>"
    }
    return output
}

function getNumbering (i, level) {
    switch ((level - 1) % 3) {
        case 1:
            return String.fromCharCode(97 + (i%26))
            break
        case 2:
        function romanise (num) {
            if (isNaN(num))
                return NaN;
            let digits = String(+num).split(""),
                key = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM",
                    "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC",
                    "","I","II","III","IV","V","VI","VII","VIII","IX"],
                roman = "",
                i = 3;
            while (i--)
                roman = (key[+digits.pop() + (i * 10)] || "") + roman;
            return Array(+digits.join("") + 1).join("M") + roman;
        }
            return romanise(i+1).toLowerCase()
            break
        default:
            return i+1
            break
    }
}

function setReferences () {
    $('a[ref]').each(function(){
        $(this).attr('href','#' + $(this).attr('ref'))
        $(this).text(refDict[$(this).attr('ref')])
    })
}