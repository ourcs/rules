# rules

## JSON Format
### Rules Object:
* `title` - **Required** - *String* - Text to display next to the numbering
* `content` - *string* - Text to display under the numbering
* `contents` - *Array of Rules Object* - Recursive rules object that indents and creates a sublist
* `continued` - *string* - Text to display after the sublist
* `ref` - *string* - UNIQUE identification of clause to be referenced

### Example
````
{
  "title": "Constitution",
  "contents": [
    {
      "title": "Purpose of this Document",
      "content": "This document shall form the Constitution of Oxford University Rowing Clubs (OURCs)."
    },
    {
      "title": "Aims of OURCs",
      "content": "The aims of the Oxford University Rowing Clubs (OURCs) are to:",
      "contents": [
        {
          "title": "Support rowing within the University;"
        },
        {
          "title": "Organise inter-college rowing competitions;"
        },
        {
          "title": "Assist member clubs to develop and maintain a safe approach to rowing;"
        }
      ]
    }
  ]
}
````
The above JSON produces (markdown doesn't support multiple tabs so pretend `-` is whitespace):
1. Constitution   
 
----1.1. Purpose of this Document

----This document shall form the Constitution of Oxford University Rowing Clubs (OURCs).

----1.2. Aims of OURCs

----The aims of the Oxford University Rowing Clubs (OURCs) are to:

--------1.2.a. Support rowing within the University;

--------1.2.b. Organise inter-college rowing competitions;

--------1.2.c. Assist member clubs to develop and maintain a safe approach to rowing;

##How to Internally Reference
N.B. Only referencing within the same document is currently supported.

To create a point to reference to, give the relevant rule a `"ref": "uniqueIdentifier"` in the JSON.

To reference to said rule, where the rule should be referenced, put `<a ref='uniqueIdentifier' />` instead of the rule number.  You can choose to not have a self closing anchor as a fallback, e.g. `<a ref='uniqueIdentifier'>1.2.c</a>`.

##Script Usage
To convert the JSON into HTML, use the function `formatJSON` under `/scripts/rules_format.js`

`formatJSON` takes two arguments, `prefix` and `data`.  `prefix` denotes the outermost numbering system.  For example, on the above example, one would pass `1` to produce `1. Constitution`.  Load the JSON into an object, and pass it into `data`.

`formatJSON` returns a string of valid HTML which can then be inserted into the DOM.

###Modifying appearance

It is possible to change the appearance of the title text and numbering system by editing the JS in `rules_format.js`.  

####Changing title appearance

To modify the title text, see the switch case.  The `wrapper` object denotes what HTML the title will be wrapped in.  Additional cases can be added to the switch to add more variance to lower levels.

####Changing numbering order

The default numbering system is [Number].*[Number].[Lower Case Alphabet].[Roman Numerals].*, repeating the italicised parts (i.e. 1.1.a.I.1.a.I.etc.)  To change the numbering, edit the cases as necessary.  N.B. the switch compares `(level-1)%3` by default which makes the first **and** second elements take the default case.  Modify as necessary if you don't want this behaviour.